package main

import (
	"flag"
	"glprojectlist/internal/application"
	"log"
	"os"
)

func main() {
	var groupID int
	flag.IntVar(&groupID, "group", 0, "the gitlab's group ID that you want to get all projects.")
	flag.Parse()

	if groupID == 0 {
		log.Fatal("you need to pass the group id like that: go run cmd/main.go -group=<id-here>")
	}

	secret := os.Getenv("GITLAB_TOKEN")

	if secret == "" {
		log.Fatal("you need to export the GITLAB_TOKEN variable in your environment")
	}

	ctx := application.CustomCtx{}
	ctx.SetGroupID(groupID)
	ctx.SetGenClient(secret)

	a := application.NewApp(ctx)

	a.RunApp()
}
