# gl-project-list

## What does this do?

Generate a file containing all projects and subprojects that belongs to the group ID provided.

## Usage

0 - Export the `GITLAB_TOKEN` variable:
```
export GITLAB_TOKEN="<your-personal-token-here>"
```

1 - Run the script:
```
go run cmd/main.go -group=<gitlab-group-id>
```

> The output will be on `output.txt` file in root of project.