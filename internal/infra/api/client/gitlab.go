package client

import (
	"fmt"
	"log"

	"gitlab.com/albuquerque53/http-go/pkg/http"
)

type GitlabClient struct {
	GenClient *http.Client
}

func (c GitlabClient) ListProjects(groupID int, page int) string {
	url := fmt.Sprintf("https://gitlab.com/api/v4/groups/%d/projects?page=%d&per_page=100", groupID, page)

	resp, err := c.GenClient.ExecuteRequest(http.GET, url, "")

	if err != nil {
		log.Fatal(err)
	}

	return resp.Body
}
