package repository

import (
	"encoding/json"
	project "glprojectlist/internal/domain"
	"glprojectlist/internal/infra/api/client"
	"log"
)

type GitlabRepo struct {
	Client *client.GitlabClient
}

func (r GitlabRepo) GetProjectsOfGroup(groupID int, page int) []*project.DTO {
	resp := r.Client.ListProjects(groupID, page)

	var projects []*project.DTO

	err := json.Unmarshal([]byte(resp), &projects)

	if err != nil {
		log.Fatal(err)
	}

	return projects
}
