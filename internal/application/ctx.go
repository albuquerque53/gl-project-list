package application

import httpgo "gitlab.com/albuquerque53/http-go/pkg/http"

type CustomCtx struct {
	genClient *httpgo.Client
	groupID   int
}

func (c *CustomCtx) SetGenClient(privateToken string) {
	h := map[string]string{
		"Content-Type":  "application/json",
		"PRIVATE-TOKEN": privateToken,
	}

	c.genClient = httpgo.NewClient(h)
}

func (c *CustomCtx) SetGroupID(groupID int) {
	c.groupID = groupID
}

func (c *CustomCtx) GetGenClient() *httpgo.Client {
	return c.genClient
}
