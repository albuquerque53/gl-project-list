package application

import (
	project "glprojectlist/internal/domain"
	"glprojectlist/internal/infra/api/client"
	"glprojectlist/internal/infra/api/repository"
)

type App struct {
	CustomCtx
}

func NewApp(ctx CustomCtx) *App {
	return &App{ctx}
}

func (a *App) RunApp() {
	gl := &client.GitlabClient{GenClient: a.GetGenClient()}
	r := repository.GitlabRepo{Client: gl}
	e := &project.Entity{Repo: r}

	e.GenerateReportOfProjects(a.groupID)
}
