package project

import (
	"fmt"
	"log"
	"os"
	"time"
)

type (
	DTO struct {
		ID     int    `json:"id"`
		Name   string `json:"name"`
		SSHUrl string `json:"ssh_url_to_repo"`
	}
	Repository interface {
		GetProjectsOfGroup(groupID int, page int) []*DTO
	}
	Entity struct {
		Repo Repository
	}
)

func (e *Entity) GenerateReportOfProjects(groupID int) {
	start := time.Now()
	file := createReportFile()
	page := 1
	count := 0

	for {
		fmt.Printf("Page: %d\n", page)
		projects := e.Repo.GetProjectsOfGroup(groupID, page)

		if len(projects) < 1 {
			dur := time.Since(start).Seconds()
			fmt.Printf("Finished! %d projects in %f seconds", count, dur)
			break
		}

		for _, project := range projects {
			fmt.Fprintln(file, project.SSHUrl)
			count++
		}

		page++
	}
}

func createReportFile() *os.File {
	f, err := os.Create("output.txt")

	if err != nil {
		log.Fatal(err)
	}

	return f
}
